//
//  main.cpp
//  MCJITCrashRepro
//
//  Created by Graham Lee on 30/06/2015.
//  Copyright (c) 2015 Graham Lee. All rights reserved.
//

#include <iostream>
#undef DEBUG
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/CodeGen/CodeGenAction.h"
#include "clang/Driver/Compilation.h"
#include "clang/Driver/Driver.h"
#include "clang/Driver/Tool.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/CompilerInvocation.h"
#include "clang/Frontend/FrontendDiagnostic.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/Bitcode/BitstreamReader.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/ExecutionEngine/MCJIT.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Path.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#pragma clang diagnostic pop

using namespace clang;
using namespace clang::driver;

static const char *source = "#import <stdio.h>\n"
"int doItMain()\n"
"{\n"
"return 42;\n"
"}\n";

bool canUseCompilerJobs(const driver::JobList &Jobs, DiagnosticsEngine &Diags)
{
  bool result = true;
  if (Jobs.size() != 1 || !isa<driver::Command>(*Jobs.begin())) {
    llvm::SmallString<256> Msg;
    llvm::raw_svector_ostream OS(Msg);
    OS << "size: " << Jobs.size();
    Jobs.Print(OS, "; ", true);
    Diags.Report(diag::err_fe_expected_compiler_job) << OS.str();
    result = false;
  }
  return result;
}

int main(int argc, const char * argv[]) {
  std::string executable_name(argv[0]);
  std::string diagnostic_output;
  llvm::raw_string_ostream ostream(diagnostic_output);
  IntrusiveRefCntPtr<DiagnosticOptions> options = new DiagnosticOptions;
  TextDiagnosticPrinter *diagnosticClient = new TextDiagnosticPrinter(ostream, &*options);
  IntrusiveRefCntPtr<DiagnosticIDs> diagnosticIDs(new DiagnosticIDs());
  DiagnosticsEngine diagnostics(diagnosticIDs, &*options, diagnosticClient);
  llvm::Triple processTriple(llvm::sys::getProcessTriple());
  Driver driver(executable_name, processTriple.str(), diagnostics);
  driver.setTitle("clang");
  driver.setCheckInputsExist(false);

  SmallVector<const char *, 16> arguments;
  const char *args[] = { "-fsyntax-only", "-x", "c",
//    "-isysroot", "/Applications/Xcode_6_3_2.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.10.sdk",
//    "-I", "/Applications/Xcode_6_3_2.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/6.1.0/include",
    "-fobjc-arc", "-c", "/tmp/empty"};
  for (int i = 0; i < 6; i++) arguments.push_back(args[i]);
  std::unique_ptr<Compilation> C(driver.BuildCompilation(arguments));
  if (!C)
  {
    std::cerr << "Couldn't build compilation" << std::endl;
    return -1;
  }
  // we should now be able to extract the list of jobs from that
  const driver::JobList &Jobs = C->getJobs();
  if (!canUseCompilerJobs(Jobs, diagnostics))
  {
    std::cerr << "Cannot use compiler jobs" << std::endl;
    return -1;
  }
  //and pull the clang invocation from the list of jobs
  driver::Command &command = cast<driver::Command>(*Jobs.begin());
  if (llvm::StringRef(command.getCreator().getName()) != "clang")
  {
    diagnostics.Report(diag::err_fe_expected_clang_command);
    return -1;
  }
  const driver::ArgStringList &CCArgs = command.getArguments();
  std::unique_ptr<CompilerInvocation> CI(new CompilerInvocation);
  CompilerInvocation::CreateFromArgs(*CI,
                                     const_cast<const char **>(CCArgs.data()),
                                     const_cast<const char **>(CCArgs.data()) +
                                     CCArgs.size(),
                                     diagnostics);
  CompilerInstance Clang;
  // Create the compilers actual diagnostics engine.
  Clang.createDiagnostics();
  if (!Clang.hasDiagnostics())
  {
    std::cerr << "Clang couldn't create diagnostics" << std::endl;
    return -1;
  }
  //create a virtual file with our content
  Clang.setInvocation(CI.release());
  Clang.createFileManager();
  Clang.createSourceManager(Clang.getFileManager());
  const FileEntry *mainFileEntry = Clang.getFileManager().getVirtualFile("/tmp/empty", strlen(source), time(0));
  llvm::StringRef sourceString(source);
  Clang.getSourceManager().overrideFileContents(mainFileEntry, std::move(llvm::MemoryBuffer::getMemBuffer(sourceString)));
  
  // Infer the builtin include path if unspecified.
  if (Clang.getHeaderSearchOpts().UseBuiltinIncludes &&
      Clang.getHeaderSearchOpts().ResourceDir.empty())
  {
    Clang.getHeaderSearchOpts().ResourceDir = getenv("HOME");
  }
  
  // Create and execute the frontend to generate an LLVM bitcode module.
  std::unique_ptr<CodeGenAction> Act(new EmitLLVMOnlyAction());
  if (!Clang.ExecuteAction(*Act))
  {
    std::cerr << "Couldn't compile the source" << std::endl;
    return -1;
  }
  
  std::unique_ptr<llvm::Module> mod = Act->takeModule();
  
  LLVMLinkInMCJIT();
  llvm::InitializeNativeTargetAsmPrinter();
  llvm::InitializeNativeTargetAsmParser();
  llvm::InitializeNativeTarget();
  std::string Error;

  llvm::raw_string_ostream outstream(Error);
  if (llvm::verifyModule(*mod, &outstream))
  {
    std::cerr << "unable to verify module: " << Error << std::endl;
    return -1;
  }
  std::string EngineBuildError;
  std::unique_ptr<llvm::ExecutionEngine> EE(llvm::EngineBuilder(std::move(mod)).setErrorStr(&EngineBuildError).create());
  if (!EE)
  {
    std::cerr << "unable to build execution engine" << std::endl;
    return -1;
  }
  EE->finalizeObject();
  llvm::Function *EntryFn = EE->FindFunctionNamed("doItMain");
  if (!EntryFn)
  {
    std::cerr << "doItMain() not found in module" << std::endl;
    return -1;
  }
  llvm::ArrayRef<llvm::GenericValue> functionArguments;
  llvm::GenericValue result = EE->runFunction(EntryFn, functionArguments);

  uint64_t returnValue = result.IntVal.getLimitedValue();
  std::cout << "result: " << returnValue << std::endl;
  
  return 0;
}
